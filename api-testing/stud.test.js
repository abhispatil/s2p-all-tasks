const sup = require('supertest');
const url = "http://109.106.255.69:2208/student/"
const url1 = "http://109.106.255.69:2208/contact/"
const url2 = "http://109.106.255.69:2208/contact/"
let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdHVkZW50SWQiOjExNSwiaWF0IjoxNjYxOTQ3MjA2LCJleHAiOjE2NjE5NTA4MDZ9.x2t7vFQCh4ZS_eU9Rq3V5ymj-nUmztH9r-fLw29Myjo"
beforeAll(() => {
    console.log("before test cases")
})

beforeEach(() => {
    console.log("start")
})

afterEach(() => {
    console.log("end")
})

afterAll(() => {
    console.log("after test cases")
})

describe("SIGNUP", () => {
    test.skip("POST signup form", async () => {
        const body = await sup(url).post("/register")
            .send(
                {
                    "studentName": "ashupatil1",
                    "phone": 9960452130,
                    "email": "ashup1@gmail.com",
                    "rollNo": "102",
                    "password": "12345"
                }
            )
        console.log("body==>", body);
        expect(body.statusCode).toBe(200);
    })
})
describe("LOGIN", () => {
    test("POST user/login", async () => {
        const body = await sup(url).post("/login")
            .send(
                {
                    "email": "shrii@gmail.com",
                    "password": "12345"
                }
            )
        console.log("body==>", body);
        expect(body.statusCode).toBe(200);
    })
})
describe("ADD CONTACT", () => {
    test.skip("update ", async () => {
        const body = await sup(url1).post("/addContact").set("x-access-token","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdHVkZW50SWQiOjExNSwiaWF0IjoxNjYxOTQ3MjA2LCJleHAiOjE2NjE5NTA4MDZ9.x2t7vFQCh4ZS_eU9Rq3V5ymj-nUmztH9r-fLw29Myjo")
            
            .send(
                {
                    "name": "ashu",
                    "phone": 9960345123,
                    "email": "ashu@gmail.com",
                    "description": "sis"
                }
            )
        console.log("body==>", body);
        expect(body.statusCode).toBe("200");    
    })
})
describe("GET ALL STUDENT", () => {
    test("get-contact", async () => {
        const body = await sup(url2).get("getAllContactByStudentId/200")
            .set('Authorization', 'bearer ' + token)
    })
})
describe("EDIT CONTACT", () => {
    test("edit ", async () => {
        const body = await sup(url1).post("/getContactById/427")
            .set('Authorization', 'bearer ' + token)
            .send(
                {
                    "name": "ashu",
                    "phone": 9960345123,
                    "email": "ashu@gmail.com",
                    "description": "sister"
                }
            )
    })
})
describe("DELETE", () => {
    test("delete", async () => {
        const body = await sup(url1).delete("/deleteContactById/427")
            .set('Authorization', 'bearer ' + token)
            .send(
                {
                    "name": "ashu",
                    "phone": 9960345123,
                    "email": "ashu@gmail.com",
                    "description": "sister"
                }
            )
    })
})
