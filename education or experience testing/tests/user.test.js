const supertest = require('supertest');
var auth = require('./token');
var url = "http://156.67.219.134:6060/api/v1/admin/";


beforeAll(async () => {
    const response = await supertest(auth.url).post('/login')
        .send({

            "email": "ashwinisunilpatil@gamil.com",
            "password": "Admin@1234",

        });
        // console.log(response.text);
    expect(response.status).toBe(200)
    auth.token = response._body.result.token
})
//-------------------STUDENT DETAIL-------------------
beforeAll(
    async () => {
        const response = await supertest(url).post('studentDetails/createStudentDetails')
            .set('Authorization', 'bearer ' + auth.token)
                .send({
                    "key": "student",
                    "name": "djhh",
                    "dob": "2002-3-7",
                    "email": "doikko@gmail.com",
                    "mobile": "8898764563",
                    "gender": "Male",
                    "stateId": "21",
                    "districtId": "386",
                    "aadharNo": "395823369887",
                    "status": "Active",
                    "hiredStatus": "false",
                    "talukaId": "173",
                    "role": "STUDENT",
                    "planPurchaseDate": "2022-9-8"
                });
            
            auth.stdid = response._body.result.studentId;
            //console.log(stdid);
            expect(response.status).toBe(200)
        }
    
)

// // -------------------SIGN UP/LOGIN-------------------
// describe('POST SIGNUP/LOGIN', function () {
//     -------------------SIGN UP/LOGIN-------------------
//     it('user signup', async () => {
//         const response = await supertest(auth.url).post('/signup')
//             .send({
//                 "userName": "abhishekspatil",
//                 "email": "abhishekspatil@gmail.com",
//                 "password": "Admin@123",
//                 "role": "SUPER ADMIN",
//             });

//         expect(response.status).toBe(200)

//     })

// })


//-------------------EDUCATION DETAILS BY STUDENT ID-------------------
describe('EDUCATION DETAILS  BY STUDENT ID', function () {
    it('get educational details by student id testcase :-1', async () => {
        const response = await supertest(url).get('education/getEducationDetailsByStudentId/'+auth.stdid).set('Authorization', 'bearer ' + auth.token)
        expect(response.status).toBe(200)
    })
})


//-------------------HIGHER EDUCATION-------------------
describe('HIGHER EDUCATION', function () {
    it('PUT higher education ITI testcase :-1', async () => {
        const response = await supertest(url).put('studentDetails/updateHigherEducation/'+auth.stdid).set('Authorization', 'bearer ' + auth.token)
            .send({
                "highestQualification": "ITI"

            });
        expect(response.status).toBe(200)
    })
    it('PUT higher education Diploma testcase :-1', async () => {
        const response = await supertest(url).put('studentDetails/updateHigherEducation/'+auth.stdid).set('Authorization', 'bearer ' + auth.token)
            .send({
                "highestQualification": "Diploma"

            });
        expect(response.status).toBe(200)
    })
    it('PUT higher education engineering testcase :-1', async () => {
        const response = await supertest(url).put('studentDetails/updateHigherEducation/'+auth.stdid).set('Authorization', 'bearer ' + auth.token)
            .send({
                "highestQualification": "Engeneering"

            });
        expect(response.status).toBe(200)
    })
})

//-------------------10TH OPTION-------------------
describe('10TH OPTION', function () {
    it('10TH OPTION testcase [correct] :-1', async () => {
        const response = await supertest(url).put('studentDetails/updateAfterEdu/'+auth.stdid).set('Authorization', 'bearer ' + auth.token)
            .send({
                "afterEdu": "10th"
            });
        expect(response.status).toBe(200)
    })
    it('10TH OPTION testcase [wrong] :-2', async () => {
        const response = await supertest(url).put('studentDetails/updateAfterEdu/'+auth.stdid).set('Authorization', 'bearer ' + auth.token)
            .send({
                "afterEdu": "15th"
            });
        expect(response.status).toBe(500)
    })
    it('10TH OPTION testcase [wrong] :-3', async () => {
        const response = await supertest(url).put('studentDetails/updateAfterEdu/'+auth.stdid).set('Authorization', 'bearer ' + auth.token)
            .send({
                "afterEdu": "11th"
            });
        expect(response.status).toBe(500)
    })
})

//-------------------12TH OPTION-------------------
describe('12TH OPTION', function () {
    it('12TH OPTION testcase [correct] :-1', async () => {
        const response = await supertest(url).put('studentDetails/updateAfterEdu/'+auth.stdid).set('Authorization', 'bearer ' + auth.token)
            .send({
                "afterEdu": "12th"
            });
        expect(response.status).toBe(200)
    })
    it('12TH OPTION testcase [wrong] :-2', async () => {
        const response = await supertest(url).put('studentDetails/updateAfterEdu/'+auth.stdid).set('Authorization', 'bearer ' + auth.token)
            .send({
                "afterEdu": "15th"
            });
        expect(response.status).toBe(500)
    })
    it('12TH OPTION testcase [wrong] :-3', async () => {
        const response = await supertest(url).put('studentDetails/updateAfterEdu/'+auth.stdid).set('Authorization', 'bearer ' + auth.token)
            .send({
                "afterEdu": "11th"
            });
        expect(response.status).toBe(500)
    })
})


//-------------------10TH DETAILS-------------------
describe('10TH DETAILS FORM ', function () {
    it('10TH OPTION testcase [correct] :-1', async () => {
        const response = await supertest(url).post('education/createEducationDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "10th",
                "yearOfPassing": " 2022-9-1",
                "percentage": "96",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
    it('10TH OPTION testcase [wrong] :-2', async () => {
        const response = await supertest(url).post('education/createEducationDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "10th",
                "yearOfPassing": " 2022-12-1",
                "percentage": "230",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
    it('10TH OPTION testcase [wrong] :-3', async () => {
        const response = await supertest(url).post('education/createEducationDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "10th",
                "yearOfPassing": " 2022-10-1",
                "percentage": "1890",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
})


describe('Getting Update Id',() => {
    it('For 10Th' , async () => {
        const response = await supertest(url)
            .get('studentDetails/getStudentAllDetailsById/'+auth.stdid)
            .set('Authorization', 'bearer ' + auth.token)
        auth.Id_10 = response._body.result.educationDetails[0].id;
       // console.log(Id_10);
        expect(response.status).toBe(200);
    })
})


// -------------------10TH DETAILS UPDATE-------------------
describe('10TH DETAILS update FORM ', function () {
    it('10TH OPTION update testcase [correct] :-1', async () => {
        const response = await supertest(url).put('education/updateEducationDetails/'+auth.Id_10).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "10th",
                "yearOfPassing": " 2022-9-1",
                "percentage": "96",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
    it('10TH OPTION update testcase [wrong] :-2', async () => {
        const response = await supertest(url).put('education/updateEducationDetails/'+auth.Id_10).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "10th",
                "yearOfPassing": " 2022-12-1",
                "percentage": "230",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
    it('10TH OPTION update testcase [wrong] :-3', async () => {
        const response = await supertest(url).put('education/updateEducationDetails/'+auth.Id_10).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "10th",
                "yearOfPassing": " 2022-10-1",
                "percentage": "1890",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
})

//-------------------12TH DETAILS-------------------
describe('12TH DETAILS FORM ', function () {
    it('12TH OPTION testcase [correct] :-1', async () => {
        const response = await supertest(url).post('education/createEducationDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "12th",
                "yearOfPassing": " 2022-9-1",
                "percentage": "96",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
    it('12TH OPTION testcase [wrong] :-2', async () => {
        const response = await supertest(url).post('education/createEducationDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "12th",
                "yearOfPassing": " 2022-12-1",
                "percentage": "230",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
    it('12TH OPTION testcase [wrong] :-3', async () => {
        const response = await supertest(url).post('education/createEducationDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "12th",
                "yearOfPassing": " 2022-10-1",
                "percentage": "1500",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
})

describe('Getting Update Id',() => {
    it('For 12Th' , async () => {
        const response = await supertest(url)
            .get('studentDetails/getStudentAllDetailsById/'+auth.stdid)
            .set('Authorization', 'bearer ' + auth.token)
        auth.Id_12 = response._body.result.educationDetails[1].id;
        //console.log(Id_12);
        expect(response.status).toBe(200);
    })
})

//-------------------12TH DETAILS UPDATE-------------------
describe('12TH DETAILS update FORM ', function () {
    it('12TH OPTION testcase [correct] :-1', async () => {
        const response = await supertest(url).put('education/updateEducationDetails/'+auth.Id_12).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "12th",
                "yearOfPassing": " 2022-9-1",
                "percentage": "96",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
    it('12TH OPTION testcase [wrong] :-2', async () => {
        const response = await supertest(url).put('education/updateEducationDetails/'+auth.Id_12).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "12th",
                "yearOfPassing": " 2022-12-1",
                "percentage": "230",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
    it('12TH OPTION testcase [wrong] :-3', async () => {
        const response = await supertest(url).put('education/updateEducationDetails/'+auth.Id_12).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "12th",
                "yearOfPassing": " 2022-10-1",
                "percentage": "1500",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
})



// -------------------ITI DETAILS-------------------
describe('ITI DETAILS FORM ', function () {
    it('ITI OPTION testcase [correct] :-1', async () => {
        const response = await supertest(url).post('itiDetails/createItiDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "ITI",
                "yearOfPassing": "2022-9-8",
                "percentage": "96",
                "trade": "Mechanic Diesel",
                "collegeId": "4174",
                "stateId": "21",
                "districtId": "386",
                "studentId": auth.stdid,
                "userId": "46"
            });
        expect(response.status).toBe(200)
    })
    it('ITI OPTION testcase [wrong] :-2', async () => {
        const response = await supertest(url).post('itiDetails/createItiDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "ITI",
                "yearOfPassing": "2022-11-8",
                "percentage": "1000",
                "trade": "Mechanic Diesel",
                "collegeId": "4174",
                "stateId": "21",
                "districtId": "386",
                "studentId": auth.stdid,
                "userId": "46"
            });
        expect(response.status).toBe(200)
    })
    it('ITI OPTION testcase [wrong] :-3', async () => {
        const response = await supertest(url).post('itiDetails/createItiDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "ITI",
                "yearOfPassing": "2022-12-8",
                "percentage": "",
                "trade": "Mechanic Diesel",
                "collegeId": "4174",
                "stateId": "21",
                "districtId": "386",
                "studentId": auth.stdid,
                "userId": "46"
            });
        expect(response.status).toBe(200)
    })
})

describe('Getting Update Id',() => {
    it('For ITI' , async () => {
        const response = await supertest(url)
            .get('studentDetails/getStudentAllDetailsById/'+auth.stdid)
            .set('Authorization', 'bearer ' + auth.token)
        auth.Id_iti = response._body.result.ITIDetails[0].id;
        //console.log(Id_iti);
        expect(response.status).toBe(200);
    })
})

//-------------------ITI DETAILS UPDATE-------------------
describe('ITI DETAILS UPDATE FORM ', function () {
    it('ITI OPTION testcase [correct] :-1', async () => {
        const response = await supertest(url).put('itiDetails/updateItiDetails/'+auth.Id_iti).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "ITI",
                "yearOfPassing": "2022-9-8",
                "percentage": "96",
                "trade": "Mechanic Diesel",
                "collegeId": "4174",
                "stateId": "21",
                "districtId": "386",
                "studentId": auth.stdid,
                "userId": "46"
            });
        expect(response.status).toBe(200)
    })
    it('ITI OPTION testcase [wrong] :-2', async () => {
        const response = await supertest(url).put('itiDetails/updateItiDetails/'+auth.Id_iti).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "ITI",
                "yearOfPassing": "2022-11-8",
                "percentage": "1000",
                "trade": "Mechanic Diesel",
                "collegeId": "4174",
                "stateId": "21",
                "districtId": "386",
                "studentId": auth.stdid,
                "userId": "46"
            });
        expect(response.status).toBe(200)
    })
    it('ITI OPTION testcase [wrong] :-3', async () => {
        const response = await supertest(url).put('itiDetails/updateItiDetails/'+auth.Id_iti).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "ITI",
                "yearOfPassing": "2022-10-8",
                "percentage": "1000",
                "trade": "Mechanic Diesel",
                "collegeId": "4174",
                "stateId": "21",
                "districtId": "386",
                "studentId": auth.stdid,
                "userId": "46"
            });
        expect(response.status).toBe(200)
    })
})



//-------------------DIPLOMA DETAILS-------------------
describe('DIPLOMA DETAILS FORM ', function () {
    it('DIPLOMA OPTION testcase [correct] :-1', async () => {
        const response = await supertest(url).post('diplomaDetails/createDiplomaDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "Diploma",
                "yearOfPassing": "2022-9-1",
                "percentage": "120+96",
                "stream": "Diploma In Handloom Technology",
                "stateId": "21",
                "districtId": "386",
                "collegeId": "5021",
                "userId": "46",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
    it('DIPLOMA OPTION testcase [wrong] :-2', async () => {
        const response = await supertest(url).post('diplomaDetails/createDiplomaDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "Diploma",
                "yearOfPassing": "2022-9-1",
                "percentage": "180",
                "stream": "Diploma In Handloom Technology",
                "stateId": "21",
                "districtId": "386",
                "collegeId": "5021",
                "userId": "46",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
    it('DIPLOMA OPTION testcase [wrong] :-3', async () => {
        const response = await supertest(url).post('diplomaDetails/createDiplomaDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "Diploma",
                "yearOfPassing": "2022-11-1",
                "percentage": "1810",
                "stream": "Diploma In Handloom Technology",
                "stateId": "21",
                "districtId": "386",
                "collegeId": "5021",
                "userId": "46",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
})

describe('Getting Update Id',() => {
    it('For Diploma' , async () => {
        const response = await supertest(url)
            .get('studentDetails/getStudentAllDetailsById/'+auth.stdid)
            .set('Authorization', 'bearer ' + auth.token)
        auth.Id_dp = response._body.result.diplomaDetails[0].id;
       // console.log(Id_dp);
        expect(response.status).toBe(200);
    })
})
//-------------------DIPLOMA DETAILS UPDATE-------------------
describe('DIPLOMA DETAILS UPDATE FORM ', function () {
    it('DIPLOMA OPTION testcase [correct] :-1', async () => {
        const response = await supertest(url).put('diplomaDetails/updateDiplomaDetails/'+auth.Id_dp).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "id": auth.Id_dp,
                "education": "Diploma",
                "yearOfPassing": "2022-9-15",
                "percentage": '90',
                "stream": "Diploma In Interior Decoration",
                "stateId": "21",
                "districtId": "386",
                "studentId": auth.stdid,
                "collegeId": "5024"
            });
        expect(response.status).toBe(200)
    })
    it('DIPLOMA OPTION testcase [wrong] :-2', async () => {
        const response = await supertest(url).put('diplomaDetails/updateDiplomaDetails/'+auth.Id_dp).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "id": auth.Id_dp,
                "education": "Diploma",
                "yearOfPassing": "2022-9-15",
                "percentage": '90',
                "stream": "Diploma In Interior Decoration",
                "stateId": "21",
                "districtId": "386",
                "studentId": auth.stdid,
                "collegeId": "5024"
            });
        expect(response.status).toBe(200)
    })
})




//-------------------ENGINEERING DETAILS-------------------
describe('ENGINEERING DETAILS FORM ', function () {
    it('ENGINEERING OPTION testcase [correct] :-1', async () => {
        const response = await supertest(url).post('engineeringDetails/createEngineeringDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "Engineering",
                "yearOfPassing": "2022-9-1",
                "percentage": "89",
                "stream": "Instrumentation Engineering",
                "stateId": "21",
                "districtId": "386",
                "studentId": auth.stdid,
                "collegeId": "5518"
            });
        expect(response.status).toBe(200)
    })
    it('ENGINEERING OPTION testcase [wrong] :-2', async () => {
        const response = await supertest(url).post('engineeringDetails/createEngineeringDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "Engineering",
                "yearOfPassing": "2022-9-1",
                "percentage": "152",
                "stream": "Instrumentation Engineering",
                "stateId": "21",
                "districtId": "386",
                "studentId": auth.stdid,
                "collegeId": "5518"
            });
        expect(response.status).toBe(200)
    })
    it('ENGINEERING OPTION testcase [wrong] :-3', async () => {
        const response = await supertest(url).post('engineeringDetails/createEngineeringDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "Engineering",
                "yearOfPassing": "2022-9-1",
                "percentage": "100*45",
                "stream": "Instrumentation Engineering",
                "stateId": "21",
                "districtId": "386",
                "studentId": auth.stdid,
                "collegeId": "5518"
            });
        expect(response.status).toBe(200)
    })
})

describe('Getting Update Id',() => {
    it('For Engineering' , async () => {
        const response = await supertest(url)
            .get('studentDetails/getStudentAllDetailsById/'+auth.stdid)
            .set('Authorization', 'bearer ' + auth.token)
        auth.Id_eng = response._body.result.engineeringDetails[0].id;
        //console.log(Id_eng);
        expect(response.status).toBe(200);
    })
})




//-------------------ENGINEERING DETAILS UPDATE-------------------
describe('ENGINEERING DETAILS UPDATE FORM ', function () {
    it('ENGINEERING OPTION testcase [correct] :-1', async () => {
        const response = await supertest(url).put('engineeringDetails/updateEngineeringDetails/'+auth.Id_eng).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "Engineering",
                "yearOfPassing": "2022-9-1",
                "percentage": "89",
                "stream": "Instrumentation Engineering",
                "stateId": "21",
                "districtId": "386",
                "studentId": auth.stdid,
                "collegeId": "5518"
            });
        expect(response.status).toBe(200)
    })
    it('ENGINEERING OPTION testcase [wrong] :-2', async () => {
        const response = await supertest(url).put('engineeringDetails/updateEngineeringDetails/'+auth.Id_eng).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "Engineering",
                "yearOfPassing": "2022-12-1",
                "percentage": "152",
                "stream": "Instrumentation Engineering",
                "stateId": "21",
                "districtId": "386",
                "studentId": auth.stdid,
                "collegeId": "5518"
            });
        expect(response.status).toBe(200)
    })
    it('ENGINEERING OPTION testcase [wrong] :-3', async () => {
        const response = await supertest(url).put('engineeringDetails/updateEngineeringDetails/'+auth.Id_eng).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "education": "Engineering",
                "yearOfPassing": "2022-10-1",
                "percentage": "100*45",
                "stream": "Instrumentation Engineering",
                "stateId": "21",
                "districtId": "386",
                "studentId": auth.stdid,
                "collegeId": "5518"
            });
        expect(response.status).toBe(200)
    })
})

//-------------------Experience-------------------
describe('EXPERIENCE', function () {
    it('EXPERIENCE testcase [correct] :-1', async () => {
        const response = await supertest(url).put('studentDetails/updateExperienceType/'+auth.stdid).set('Authorization', 'bearer ' + auth.token)
            .send({
                "experienceType": "Experience"
            });
        expect(response.status).toBe(200)
    })
    it('EXPERIENCE testcase [wrong] :-2', async () => {
        const response = await supertest(url).put('studentDetails/updateExperienceType/'+auth.stdid).set('Authorization', 'bearer ' + auth.token)
            .send({
                "experienceType": "Expience"
            });
        expect(response.status).toBe(500)
    })
})


//-------------------FRESHER-------------------
describe('FRESHER', function () {
    it('FRESHER testcase [correct] :-1', async () => {
        const response = await supertest(url).put('studentDetails/updateExperienceType/'+auth.stdid).set('Authorization', 'bearer ' + auth.token)
            .send({
                "experienceType": "Fresher"
            });
        expect(response.status).toBe(200)
    })
    it('FRESHER testcase [wrong] :-2', async () => {
        const response = await supertest(url).put('studentDetails/updateExperienceType/'+auth.stdid).set('Authorization', 'bearer ' + auth.token)
            .send({
                "experienceType": "Fsher"
            });
        expect(response.status).toBe(500)
    })
})

//-------------------Experience Form-------------------
describe('Experience Form', function () {
    it('Experience Form testcase [correct] :-1', async () => {
        const response = await supertest(url).post('experienceDetails/createExperienceDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "expType": "Experience",
                "companyName": "S2P",
                "fromDate": "2022-9-1",
                "toDate": "2022-9-1",
                "salary": "120000",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
    it('Experience Form testcase [wrong] :-2', async () => {
        const response = await supertest(url).post('experienceDetails/createExperienceDetails').set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "expType": "Experience",
                "companyName": "@...S2P",
                "fromDate": "2022-9-1",
                "toDate": "2022-9-1",
                "salary": "100/20",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
})

describe('Getting Update Id',() => {
    it('For Engineering' , async () => {
        const response = await supertest(url)
            .get('studentDetails/getStudentAllDetailsById/'+auth.stdid)
            .set('Authorization', 'bearer ' + auth.token)
        auth.Id_exp = response._body.result.experienceDetails[0].id;
        //console.log(auth.Id_exp);
        expect(response.status).toBe(200);
    })
})


//-------------------Experience Form update-------------------
describe('Experience Form Update', function () {
    it('Experience Form testcase [correct] :-1', async () => {
        const response = await supertest(url).put('experienceDetails/updateExperienceDetails/'+auth.Id_exp).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "expType": "Experience",
                "companyName": "S2P",
                "fromDate": "2022-9-1",
                "toDate": "2022-9-1",
                "salary": "120000",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
    it('Experience Form testcase [wrong] :-2', async () => {
        const response = await supertest(url).put('experienceDetails/updateExperienceDetails/'+auth.Id_exp).set('Authorization', 'bearer ' + auth.token)
            .send({
                "key": "education",
                "expType": "Experience",
                "companyName": "@...S2P",
                "fromDate": "2022-12-1",
                "toDate": "2022-12-6",
                "salary": "100/20",
                "studentId": auth.stdid
            });
        expect(response.status).toBe(200)
    })
})

// // //-------------------DELETE-------------------
describe('DELETE BUTTON', function () {
    it('DELETE 12th details testcase [correct] :-1', async () => {
        const response = await supertest(url).delete('education/deleteEducationDetails/'+auth.Id_10).set('Authorization', 'bearer ' + auth.token)
        expect(response.status).toBe(200)
    })
    it('DELETE 10th details testcase [correct] :-1', async () => {
        const response = await supertest(url).delete('education/deleteEducationDetails/'+auth.Id_12).set('Authorization', 'bearer ' + auth.token)
        expect(response.status).toBe(200)
    })
    it('DELETE ITI details testcase [correct] :-1', async () => {
        const response = await supertest(url).delete('itiDetails/deleteItiDetails/'+auth.Id_iti).set('Authorization', 'bearer ' + auth.token)
        expect(response.status).toBe(200)
    })
    it('DELETE Experience details testcase [correct] :-1', async () => {
        const response = await supertest(url).delete('experienceDetails/deleteExperienceDetails/'+auth.Id_exp).set('Authorization', 'bearer ' + auth.token)
        expect(response.status).toBe(200)
    })
    it('DELETE Diploma details testcase [correct] :-1', async () => {
        const response = await supertest(url).delete('diplomaDetails/deleteDiplomaDetails/'+auth.Id_dp).set('Authorization', 'bearer ' + auth.token)
        expect(response.status).toBe(200)
    })
    it('DELETE Engineering details testcase [correct] :-1', async () => {
        const response = await supertest(url).delete('engineeringDetails/deleteEngineeringDetails/'+auth.Id_eng).set('Authorization', 'bearer ' + auth.token)
        expect(response.status).toBe(200)
    })
})




afterAll(async () => {
    const resposne = await supertest(url).delete('studentDetails/deleteStudentDetails/'+auth.stdid)
    .set('Authorization', 'bearer ' + auth.token)
    expect(resposne.status).toBe(200);
})