{
    var zomato = {
        "h1": "Welcome to Zomato!",
        "h2": "Climate Conscious Delivery",
        "h3": "Your safty is top most priorty !",
        "dominos":
        {

            "mainh1": "Domino's Pizza ",
            "mainh2": "Pizaa, Fast Food, Beverages",
            "location": "Higna T-point",
            "rating": "4.2",
            "reviews": "8,915 reviews",
            "vgng": "Veg / Non-veg",
            "recommended": {
                "heading": "Recommended",
                "pizza": [
                    {
                        "pizza": "FarmHouse",
                        "sale": "Best Seller",
                        "vgnonveg": "veg",
                        "pizzah1": "Delightfull combination of onion, capsicum, tomato & grilled mushroom ",
                        "price": "269",
                        "rating": "4.4",
                        "crust": {
                            "h1": "Crust",
                            "crust1": "Select 1 out of 5 options ",
                            "choice": [
                                {
                                    "pizza": "FarmHouse",
                                    "type": "classichand",
                                    "classichand": ["regular"],
                                    "rs": [269]
                                },
                                {
                                    "pizza": "FarmHouse",
                                    "type": "newhandtoasted",
                                    "newhandtossed": ["regular", "large", "medium"],
                                    "rs": [269, 719, 479]
                                },
                                {
                                    "pizza": "FarmHouse",
                                    "type": "cheesebrust",
                                    "cheesebrust": ["regular", "medium"],
                                    "rs": [344, 578]
                                },
                                {
                                    "pizza": "FarmHouse",
                                    "type": "freshpanpizza",
                                    "freshpanpizza": ["regular", "medium"],
                                    "rs": [299, 519]
                                },
                                {
                                    "pizza": "FarmHouse",
                                    "type": "wheatthincrust",
                                    "WheatThinCrust": ["regular", "medium"],
                                    "rs": [299, 529]
                                }
                            ],
                        },
                        "toppings": {
                            "h1": "Toppings",
                            "toppings": "Select upto 15 options",
                            "choice": [
                                "Crisp Capsicum",
                                "Fresh Tomato",
                                "Panner",
                                "Jalapeno",
                                "Pepper Barbecue Chicken",
                                "Peri-Peri Chicken"
                            ],
                            "rs": [35, 35, 35, 35, 50, 50]
                        }
                    },
                    {
                        "pizza": "Pepper Barbecue Chicken",
                        "sale": "Best Seller",
                        "vgnonveg": "non-veg",
                        "pizzah1": "Pepper barbecue chicken for that extra zing",
                        "price": "259",
                        "rating": "4.2",
                        "crust": {
                            "h1": "Crust",
                            "crust": "Select 1 out of 5 options ",
                            "choice": [
                                {
                                    "pizza": "Pepper Barbecue Chicken",
                                    "type": "classichand",
                                    "classichand": ["regular"],
                                    "rs": [259]
                                },
                                {
                                    "pizza": "Pepper Barbecue Chicken",
                                    "type": "newhandtoasted",
                                    "newhandtossed": ["regular", "large", "medium"],
                                    "rs": [259, 699, 469]
                                },
                                {
                                    "pizza": "Pepper Barbecue Chicken",
                                    "type": "cheesebrust",
                                    "cheesebrust": ["regular", "medium"],
                                    "rs": [334, 568]
                                },
                                {
                                    "pizza": "Pepper Barbecue Chicken",
                                    "type": "freshpanpizza",
                                    "freshpanpizza": ["regular", "medium"],
                                    "rs": [289, 509]
                                },
                                {
                                    "pizza": "Pepper Barbecue Chicken",
                                    "type": "wheatthincrust",
                                    "WheatThinCrust": ["regular", "medium"],
                                    "rs": [289, 519]
                                }
                            ],
                        },
                        "toppings": {
                            "h1": "Toppings",
                            "toppings": "Select upto 15 options",
                            "choice": [
                                "Crisp Capsicum",
                                "Fresh Tomato",
                                "Panner",
                                "Jalapeno",
                                "Pepper Barbecue Chicken",
                                "Peri-Peri Chicken"
                            ],
                            "rs": [35, 35, 35, 35, 50, 50]
                        }
                    },
                    {
                        "pizza": "Veg Extravaganza",
                        "vgnonveg": "veg",
                        "pizzah1": "Black Olives, capsicum , onion, grilled mushroom, corn, tomato, jalapeno & extra cheese ",
                        "price": "319",
                        "rating": "2.3",
                        "crust": {
                            "h1": "Crust",
                            "crust1": "Select 1 out of 5 options ",
                            "choice": [
                                {
                                    "pizza": "Veg Extravaganza",
                                    "type": "classichand",
                                    "classichand": ["regular"],
                                    "rs": [319]
                                },
                                {
                                    "pizza": "Veg Extravaganza",
                                    "type": "newhandtoasted",
                                    "newhandtossed": ["regular", "large", "medium"],
                                    "rs": [319, 839, 579]
                                },
                                {
                                    "pizza": "Veg Extravaganza",
                                    "type": "cheesebrust",
                                    "cheesebrust": ["regular", "medium"],
                                    "rs": [349, 678]
                                },
                                {
                                    "pizza": "Veg Extravaganza",
                                    "type": "freshpanpizza",
                                    "freshpanpizza": ["regular", "medium"],
                                    "rs": [349, 619]
                                },
                                {
                                    "pizza": "Veg Extravaganza",
                                    "type": "wheatthincrust",
                                    "WheatThinCrust": ["regular", "medium"],
                                    "rs": [349, 629]
                                }
                            ],
                        },
                        "toppings": {
                            "h1": "Toppings",
                            "toppings": "Select upto 15 options",
                            "choice": [
                                "Crisp Capsicum",
                                "Fresh Tomato",
                                "Panner",
                                "Jalapeno",
                                "Pepper Barbecue Chicken",
                                "Peri-Peri Chicken"
                            ],
                            "rs": [35, 35, 35, 35, 50, 50]
                        }
                    },

                ]
            }
        }
    }
}

console.log("------------------------------------------");
console.log(zomato.h1);
console.log(zomato.h2);
console.log(zomato.h3);
console.log("------------------------------------------");
console.log(zomato.dominos.mainh1);
console.log(zomato.dominos.mainh2);
console.log("------------------------------------------");
console.log(zomato.dominos.location);
console.log(zomato.dominos.rating);
console.log(zomato.dominos.reviews);
console.log("------------------------------------------");
console.log(zomato.dominos.vgng);
console.log(zomato.dominos.recommended.heading);
console.log("------------------------------------------");
var crusttotal = 0;
var toppingstotal = 0;
var total = 0;

const ps = require("prompt-sync");
const prompt = ps();
let nfilter = prompt("do you want to filter the pizza's crust (yes/no):- ");
if (nfilter == "yes") {
    console.log("List of Crust :- ");
    console.log("1. classichand ");
    console.log("2. newhandtoasted ");
    console.log("3. cheesebrust ");
    console.log("4. freshpanpizza ");
    console.log("5. wheatthincrust ");
    let ofilter = prompt("enter the crust :- ");
    for (i = 0; i < 3; i++) {
        console.log(zomato.dominos.recommended.pizza[i].crust.choice.filter(x => x.type == ofilter));
    }
}

for (i = 0; i < 10; i++) {
    console.log("List of pizzas :-");
    console.log("1. Farmhouse pizza ");
    console.log("2. Pepper Barbecue Chicken");
    console.log("3. Veg Extravaganza");
    console.log("------------------------------------------");
    let pname = prompt("Select pizza :- ");
    console.log("------------------------------------------");
    if (pname == "farmhouse") {
        console.log(zomato.dominos.recommended.pizza[0].pizza);
        console.log(zomato.dominos.recommended.pizza[0].sale);
        console.log(zomato.dominos.recommended.pizza[0].vgnonveg);
        console.log(zomato.dominos.recommended.pizza[0].pizzah1);
        console.log(zomato.dominos.recommended.pizza[0].price);
        console.log(zomato.dominos.recommended.pizza[0].rating);
        console.log(zomato.dominos.recommended.pizza[0].crust.h1);
        console.log("------------------------------------------");
        console.log("List of Crust :- ");
        console.log("1. Classic Hand ");
        console.log("2. New Hand Toasted ");
        console.log("3. Cheese Brust ");
        console.log("4. Fresh Pan Pizza ");
        console.log("5. Wheat Thin Crust ");
        console.log("------------------------------------------");
        let opcrust = prompt("Select crust :- ");
        console.log("------------------------------------------");
        if (opcrust == "classic hand") {
            console.log("Size :- ", zomato.dominos.recommended.pizza[0].crust.choice[0].classichand[0]);
            console.log("price :-", zomato.dominos.recommended.pizza[0].crust.choice[0].rs[0]);
            temp = zomato.dominos.recommended.pizza[0].crust.choice[0].rs[0];
            crusttotal += temp;
        }
        else if (opcrust == "new hand toasted") {
            console.log("1. Classic hand :- 'Regular'");
            console.log("2. New hand toasted :- 'Regular','Large','Medium'");
            console.log("3. Cheese brust :- 'Regular','Medium'");
            console.log("4. Fresh pan pizza :- 'Regular','Medium'");
            console.log("5. Wheat thin crust :- 'Regular','Medium'");
            console.log("------------------------------------------");
            let psize = prompt("Select size :- ");
            console.log("------------------------------------------");
            for (i = 0; i < 3; i++) {
                if (psize == zomato.dominos.recommended.pizza[0].crust.choice[1].newhandtossed[i]) {
                    console.log("Size :- ", zomato.dominos.recommended.pizza[0].crust.choice[1].newhandtossed[i]);
                    console.log("price :-", zomato.dominos.recommended.pizza[0].crust.choice[1].rs[i]);
                    temp = zomato.dominos.recommended.pizza[0].crust.choice[1].rs[i];
                    crusttotal += temp;
                }
            }
        }
        else if (opcrust == "cheese brust") {
            console.log("1. Classic hand :- 'Regular'");
            console.log("2. New hand toasted :- 'Regular','Large','Medium'");
            console.log("3. Cheese brust :- 'Regular','Medium'");
            console.log("4. Fresh pan pizza :- 'Regular','Medium'");
            console.log("5. Wheat thin crust :- 'Regular','Medium'");
            console.log("------------------------------------------");
            let psize = prompt("Select size :- ");
            console.log("------------------------------------------");
            for (i = 0; i < 2; i++) {
                if (psize == zomato.dominos.recommended.pizza[0].crust.choice[2].cheesebrust[i]) {
                    console.log("Size :- ", zomato.dominos.recommended.pizza[0].crust.choice[2].cheesebrust[i]);
                    console.log("price :-", zomato.dominos.recommended.pizza[0].crust.choice[2].rs[i]);
                    temp = zomato.dominos.recommended.pizza[0].crust.choice[2].rs[i];
                    crusttotal += temp;
                }
            }
        }
        else if (opcrust == "fresh pan pizza") {
            console.log("1. Classic hand :- 'Regular'");
            console.log("2. New hand toasted :- 'Regular','Large','Medium'");
            console.log("3. Cheese brust :- 'Regular','Medium'");
            console.log("4. Fresh pan pizza :- 'Regular','Medium'");
            console.log("5. Wheat thin crust :- 'Regular','Medium'");
            console.log("------------------------------------------");
            let psize = prompt("Select size :- ");
            console.log("------------------------------------------");
            for (i = 0; i < 2; i++) {
                if (psize == zomato.dominos.recommended.pizza[0].crust.choice[3].freshpanpizza[i]) {
                    console.log("Size :- ", zomato.dominos.recommended.pizza[0].crust.choice[3].freshpanpizza[i]);
                    console.log("price :-", zomato.dominos.recommended.pizza[0].crust.choice[3].rs[i]);
                    temp = zomato.dominos.recommended.pizza[0].crust.choice[3].rs[i];
                    crusttotal += temp;
                }
            }
        }
        else if (opcrust == "wheat thin crust") {
            console.log("1. Classic hand :- 'Regular'");
            console.log("2. New hand toasted :- 'Regular','Large','Medium'");
            console.log("3. Cheese brust :- 'Regular','Medium'");
            console.log("4. Fresh pan pizza :- 'Regular','Medium'");
            console.log("5. Wheat thin crust :- 'Regular','Medium'");
            console.log("------------------------------------------");
            let psize = prompt("Select size :- ");
            console.log("------------------------------------------");
            for (i = 0; i < 2; i++) {
                if (psize == zomato.dominos.recommended.pizza[0].crust.choice[4].WheatThinCrust[i]) {
                    console.log("Size :- ", zomato.dominos.recommended.pizza[0].crust.choice[4].WheatThinCrust[i]);
                    console.log("price :-", zomato.dominos.recommended.pizza[0].crust.choice[4].rs[i]);
                    temp = zomato.dominos.recommended.pizza[0].crust.choice[4].rs[i];
                    crusttotal += temp;
                }
            }
        }
        console.log("------------------------------------------");
        console.log("List of toopings :- ");
        console.log("1. Crisp Capsicum 35rs");
        console.log("2. Fresh Tomato 35rs");
        console.log("3. Panner 35rs");
        console.log("4. Jalapeno 35rs");
        console.log("5. Pepper Barbecue Chicken 50rs");
        console.log("6. Peri-Peri Chicken 50rs");
        console.log("7. exit");
        console.log("------------------------------------------");
        for (i = 0; i < 6; i++) {
            let ltop = prompt("Select Toppings (number):- ");
            if (ltop == "1") {
                console.log("Crisp Capsicum added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[0].toppings.rs[0];
                toppingstotal += temp1;

            }
            else if (ltop == 2) {
                console.log("Fresh Tomato added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[0].toppings.rs[1];
                toppingstotal += temp1;

            }
            else if (ltop == 3) {
                console.log("Panner added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[0].toppings.rs[2];
                toppingstotal += temp1;

            }
            else if (ltop == 4) {
                console.log("Jalapeno added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[0].toppings.rs[3];
                toppingstotal += temp1;

            }
            else if (ltop == 5) {
                console.log("Pepper Barbecue Chicken added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[0].toppings.rs[4];
                toppingstotal += temp1;

            }
            else if (ltop == 6) {
                console.log("Peri-Peri Chicken added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[0].toppings.rs[5];
                toppingstotal += temp1;

            }
            else if (ltop == 7) {
                break;
            }
            else {
                console.log("invalid option !!");
            }
        }

        total = crusttotal + toppingstotal;
        console.log("------------------------------------------");
        console.log("TOTAL :- ", total);
        console.log("------------------------------------------");
        let ask = prompt("Do you want more pizza's (yes/no) :- ");
        if (ask == "yes") {
            continue;
        }
        else {
            console.log("Thank you for visiting dominos !!!");
            break;
        }
    }
    else if (pname == "pepper barbecue chicken") {
        console.log(zomato.dominos.recommended.pizza[1].pizza);
        console.log(zomato.dominos.recommended.pizza[1].vgnonveg);
        console.log(zomato.dominos.recommended.pizza[1].pizzah1);
        console.log(zomato.dominos.recommended.pizza[1].price);
        console.log(zomato.dominos.recommended.pizza[1].rating);
        console.log(zomato.dominos.recommended.pizza[1].crust.h1);
        console.log("------------------------------------------");
        console.log("List of Crust :- ");
        console.log("1. Classic Hand ");
        console.log("2. New Hand Toasted ");
        console.log("3. Cheese Brust ");
        console.log("4. Fresh Pan Pizza ");
        console.log("5. Wheat Thin Crust ");
        console.log("------------------------------------------");
        let opcrust = prompt("Select crust :- ");
        console.log("------------------------------------------");
        if (opcrust == "classic hand") {
            console.log("Size :- ", zomato.dominos.recommended.pizza[1].crust.choice[0].classichand[0]);
            console.log("price :-", zomato.dominos.recommended.pizza[1].crust.choice[0].rs[0]);
            temp = zomato.dominos.recommended.pizza[1].crust.choice[0].rs[0];
            crusttotal += temp;
        }
        else if (opcrust == "new hand toasted") {
            console.log("1. Classic hand :- 'Regular'");
            console.log("2. New hand toasted :- 'Regular','Large','Medium'");
            console.log("3. Cheese brust :- 'Regular','Medium'");
            console.log("4. Fresh pan pizza :- 'Regular','Medium'");
            console.log("5. Wheat thin crust :- 'Regular','Medium'");
            console.log("------------------------------------------");
            let psize = prompt("Select size :- ");
            console.log("------------------------------------------");
            for (i = 0; i < 3; i++) {
                if (psize == zomato.dominos.recommended.pizza[1].crust.choice[1].newhandtossed[i]) {
                    console.log("Size :- ", zomato.dominos.recommended.pizza[1].crust.choice[1].newhandtossed[i]);
                    console.log("price :-", zomato.dominos.recommended.pizza[1].crust.choice[1].rs[i]);
                    temp = zomato.dominos.recommended.pizza[1].crust.choice[1].rs[i];
                    crusttotal += temp;
                }
            }
        }
        else if (opcrust == "cheese brust") {
            console.log("1. Classic hand :- 'Regular'");
            console.log("2. New hand toasted :- 'Regular','Large','Medium'");
            console.log("3. Cheese brust :- 'Regular','Medium'");
            console.log("4. Fresh pan pizza :- 'Regular','Medium'");
            console.log("5. Wheat thin crust :- 'Regular','Medium'");
            console.log("------------------------------------------");
            let psize = prompt("Select size :- ");
            console.log("------------------------------------------");
            for (i = 0; i < 2; i++) {
                if (psize == zomato.dominos.recommended.pizza[1].crust.choice[2].cheesebrust[i]) {
                    console.log("Size :- ", zomato.dominos.recommended.pizza[1].crust.choice[2].cheesebrust[i]);
                    console.log("price :-", zomato.dominos.recommended.pizza[1].crust.choice[2].rs[i]);
                    temp = zomato.dominos.recommended.pizza[1].crust.choice[2].rs[i];
                    crusttotal += temp;
                }
            }
        }
        else if (opcrust == "fresh pan pizza") {
            console.log("1. Classic hand :- 'Regular'");
            console.log("2. New hand toasted :- 'Regular','Large','Medium'");
            console.log("3. Cheese brust :- 'Regular','Medium'");
            console.log("4. Fresh pan pizza :- 'Regular','Medium'");
            console.log("5. Wheat thin crust :- 'Regular','Medium'");
            console.log("------------------------------------------");
            let psize = prompt("Select size :- ");
            console.log("------------------------------------------");
            for (i = 0; i < 2; i++) {
                if (psize == zomato.dominos.recommended.pizza[1].crust.choice[3].freshpanpizza[i]) {
                    console.log("Size :- ", zomato.dominos.recommended.pizza[1].crust.choice[3].freshpanpizza[i]);
                    console.log("price :-", zomato.dominos.recommended.pizza[1].crust.choice[3].rs[i]);
                    temp = zomato.dominos.recommended.pizza[1].crust.choice[3].rs[i];
                    crusttotal += temp;
                }
            }
        }
        else if (opcrust == "wheat thin crust") {
            console.log("1. Classic hand :- 'Regular'");
            console.log("2. New hand toasted :- 'Regular','Large','Medium'");
            console.log("3. Cheese brust :- 'Regular','Medium'");
            console.log("4. Fresh pan pizza :- 'Regular','Medium'");
            console.log("5. Wheat thin crust :- 'Regular','Medium'");
            console.log("------------------------------------------");
            let psize = prompt("Select size :- ");
            console.log("------------------------------------------");
            for (i = 0; i < 2; i++) {
                if (psize == zomato.dominos.recommended.pizza[1].crust.choice[4].WheatThinCrust[i]) {
                    console.log("Size :- ", zomato.dominos.recommended.pizza[1].crust.choice[4].WheatThinCrust[i]);
                    console.log("price :-", zomato.dominos.recommended.pizza[1].crust.choice[4].rs[i]);
                    temp = zomato.dominos.recommended.pizza[1].crust.choice[4].rs[i];
                    crusttotal += temp;
                }
            }
        }
        console.log("------------------------------------------");
        console.log("List of toopings :- ");
        console.log("1. Crisp Capsicum 35rs");
        console.log("2. Fresh Tomato 35rs");
        console.log("3. Panner 35rs");
        console.log("4. Jalapeno 35rs");
        console.log("5. Pepper Barbecue Chicken 50rs");
        console.log("6. Peri-Peri Chicken 50rs");
        console.log("7. exit");
        console.log("------------------------------------------");
        for (i = 0; i < 6; i++) {
            let ltop = prompt("Select Toppings (number):- ");
            if (ltop == "1") {
                console.log("Crisp Capsicum added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[1].toppings.rs[0];
                toppingstotal += temp1;

            }
            else if (ltop == 2) {
                console.log("Fresh Tomato added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[1].toppings.rs[1];
                toppingstotal += temp1;

            }
            else if (ltop == 3) {
                console.log("Panner added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[1].toppings.rs[2];
                toppingstotal += temp1;

            }
            else if (ltop == 4) {
                console.log("Jalapeno added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[1].toppings.rs[3];
                toppingstotal += temp1;

            }
            else if (ltop == 5) {
                console.log("Pepper Barbecue Chicken added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[1].toppings.rs[4];
                toppingstotal += temp1;

            }
            else if (ltop == 6) {
                console.log("Peri-Peri Chicken added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[1].toppings.rs[5];
                toppingstotal += temp1;

            }
            else if (ltop == 7) {
                break;
            }
            else {
                console.log("invalid option!!");
            }
        }
        total = crusttotal + toppingstotal;
        console.log("------------------------------------------");
        console.log("TOTAL :- ", total);
        console.log("------------------------------------------");
        let ask = prompt("Do you want more pizza's (yes/no) :- ");
        if (ask == "yes") {
            continue;
        }
        else {
            console.log("Thank you for visiting dominos !!!");
            break;
        }
    }
    else if (pname == "veg extravaganza") {
        console.log(zomato.dominos.recommended.pizza[2].pizza);
        console.log(zomato.dominos.recommended.pizza[2].sale);
        console.log(zomato.dominos.recommended.pizza[2].vgnonveg);
        console.log(zomato.dominos.recommended.pizza[2].pizzah1);
        console.log(zomato.dominos.recommended.pizza[2].price);
        console.log(zomato.dominos.recommended.pizza[2].rating);
        console.log(zomato.dominos.recommended.pizza[2].crust.h1);
        console.log("------------------------------------------");
        console.log("List of Crust :- ");
        console.log("1. Classic Hand ");
        console.log("2. New Hand Toasted ");
        console.log("3. Cheese Brust ");
        console.log("4. Fresh Pan Pizza ");
        console.log("5. Wheat Thin Crust ");
        console.log("------------------------------------------");
        let opcrust = prompt("Select crust :- ");
        console.log("------------------------------------------");
        if (opcrust == "classic hand") {
            console.log("Size :- ", zomato.dominos.recommended.pizza[2].crust.choice[0].classichand[0]);
            console.log("price :-", zomato.dominos.recommended.pizza[2].crust.choice[0].rs[0]);
            temp = zomato.dominos.recommended.pizza[2].crust.choice[0].rs[0];
            crusttotal += temp;
        }
        else if (opcrust == "new hand toasted") {
            console.log("1. Classic hand :- 'Regular'");
            console.log("2. New hand toasted :- 'Regular','Large','Medium'");
            console.log("3. Cheese brust :- 'Regular','Medium'");
            console.log("4. Fresh pan pizza :- 'Regular','Medium'");
            console.log("5. Wheat thin crust :- 'Regular','Medium'");
            console.log("------------------------------------------");
            let psize = prompt("Select size :- ");
            console.log("------------------------------------------");
            for (i = 0; i < 3; i++) {
                if (psize == zomato.dominos.recommended.pizza[2].crust.choice[1].newhandtossed[i]) {
                    console.log("Size :- ", zomato.dominos.recommended.pizza[2].crust.choice[1].newhandtossed[i]);
                    console.log("price :-", zomato.dominos.recommended.pizza[2].crust.choice[1].rs[i]);
                    temp = zomato.dominos.recommended.pizza[2].crust.choice[1].rs[i];
                    crusttotal += temp;
                }
            }
        }
        else if (opcrust == "cheese brust") {
            console.log("1. Classic hand :- 'Regular'");
            console.log("2. New hand toasted :- 'Regular','Large','Medium'");
            console.log("3. Cheese brust :- 'Regular','Medium'");
            console.log("4. Fresh pan pizza :- 'Regular','Medium'");
            console.log("5. Wheat thin crust :- 'Regular','Medium'");
            console.log("------------------------------------------");
            let psize = prompt("Select size :- ");
            console.log("------------------------------------------");
            for (i = 0; i < 2; i++) {
                if (psize == zomato.dominos.recommended.pizza[2].crust.choice[2].cheesebrust[i]) {
                    console.log("Size :- ", zomato.dominos.recommended.pizza[2].crust.choice[2].cheesebrust[i]);
                    console.log("price :-", zomato.dominos.recommended.pizza[2].crust.choice[2].rs[i]);
                    temp = zomato.dominos.recommended.pizza[2].crust.choice[2].rs[i];
                    crusttotal += temp;
                }
            }
        }
        else if (opcrust == "fresh pan pizza") {
            console.log("1. Classic hand :- 'Regular'");
            console.log("2. New hand toasted :- 'Regular','Large','Medium'");
            console.log("3. Cheese brust :- 'Regular','Medium'");
            console.log("4. Fresh pan pizza :- 'Regular','Medium'");
            console.log("5. Wheat thin crust :- 'Regular','Medium'");
            console.log("------------------------------------------");
            let psize = prompt("Select size :- ");
            console.log("------------------------------------------");
            for (i = 0; i < 2; i++) {
                if (psize == zomato.dominos.recommended.pizza[2].crust.choice[3].freshpanpizza[i]) {
                    console.log("Size :- ", zomato.dominos.recommended.pizza[2].crust.choice[3].freshpanpizza[i]);
                    console.log("price :-", zomato.dominos.recommended.pizza[2].crust.choice[3].rs[i]);
                    temp = zomato.dominos.recommended.pizza[2].crust.choice[3].rs[i];
                    crusttotal += temp;
                }
            }
        }
        else if (opcrust == "wheat thin crust") {
            console.log("1. Classic hand :- 'Regular'");
            console.log("2. New hand toasted :- 'Regular','Large','Medium'");
            console.log("3. Cheese brust :- 'Regular','Medium'");
            console.log("4. Fresh pan pizza :- 'Regular','Medium'");
            console.log("5. Wheat thin crust :- 'Regular','Medium'");
            console.log("------------------------------------------");
            let psize = prompt("Select size :- ");
            console.log("------------------------------------------");
            for (i = 0; i < 2; i++) {
                if (psize == zomato.dominos.recommended.pizza[2].crust.choice[4].WheatThinCrust[i]) {
                    console.log("Size :- ", zomato.dominos.recommended.pizza[2].crust.choice[4].WheatThinCrust[i]);
                    console.log("price :-", zomato.dominos.recommended.pizza[2].crust.choice[4].rs[i]);
                    temp = zomato.dominos.recommended.pizza[2].crust.choice[4].rs[i];
                    crusttotal += temp;
                }
            }
        }
        console.log("------------------------------------------");
        console.log("List of toopings :- ");
        console.log("1. Crisp Capsicum 35rs");
        console.log("2. Fresh Tomato 35rs");
        console.log("3. Panner 35rs");
        console.log("4. Jalapeno 35rs");
        console.log("5. Pepper Barbecue Chicken 50rs");
        console.log("6. Peri-Peri Chicken 50rs");
        console.log("7. exit");
        console.log("------------------------------------------");
        for (i = 0; i < 6; i++) {
            let ltop = prompt("Select Toppings (number):- ");
            if (ltop == "1") {
                console.log("Crisp Capsicum added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[2].toppings.rs[0];
                toppingstotal += temp1;

            }
            else if (ltop == 2) {
                console.log("Fresh Tomato added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[2].toppings.rs[1];
                toppingstotal += temp1;

            }
            else if (ltop == 3) {
                console.log("Panner added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[2].toppings.rs[2];
                toppingstotal += temp1;

            }
            else if (ltop == 4) {
                console.log("Jalapeno added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[2].toppings.rs[3];
                toppingstotal += temp1;

            }
            else if (ltop == 5) {
                console.log("Pepper Barbecue Chicken added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[2].toppings.rs[4];
                toppingstotal += temp1;

            }
            else if (ltop == 6) {
                console.log("Peri-Peri Chicken added succesfully");
                console.log("------------------------------------------");
                temp1 = zomato.dominos.recommended.pizza[2].toppings.rs[5];
                toppingstotal += temp1;

            }
            else if (ltop == 7) {
                break;
            }
            else {
                console.log("invalid option!!");
            }
        }
        total = crusttotal + toppingstotal;
        console.log("------------------------------------------");
        console.log("TOTAL :- ", total);
        console.log("------------------------------------------");
        let ask = prompt("Do you want more pizza's (yes/no) :- ");
        if (ask == "yes") {
            continue;
        }
        else {
            console.log("Thank you for visiting dominos !!!");
            break;
        }
    }
}
